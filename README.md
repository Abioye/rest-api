# REST-app API Proxy

recipe REST api application

## Usage

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

### Envionment Variable

* `LISTEN_PORT` - Port to listen on will be stated here but the defaut port is `8000` 
* `APP_HOST` - Hostname of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to forward request to (default: `9000`)